package advanced;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class AccountParser {
  
  private static final Logger log = LoggerFactory.getLogger(Transaction.class);

  public Account parse(String source){
    
    Account result = new Account();
    try (FileInputStream input = new FileInputStream(source)) {
      
      ObjectMapper mapper = new ObjectMapper();
      
      result = mapper.readValue(input, Account.class);
      
    } catch (FileNotFoundException e) {
      log.error("Can't get access to Account {}",source,e);
    } catch (Exception e) {
      log.error("Broken Account File. Check file {}",source,e);
    } 
    
    return result;
    
  }
  
  
  
  
  
}
