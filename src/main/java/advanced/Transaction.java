package advanced;

import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Transaction implements Runnable {

  private static final Logger log = LoggerFactory.getLogger(Transaction.class);
  
  Shell id1;
  Shell id2;
  Long amount;

  public Transaction(Shell id1, Shell id2, Long amount) {
    this.id1 = id1;
    this.id2 = id2;
    this.amount = amount;
  }

  @Override
  public void run() {

    if (id1 == id2) {
      log.info("Uselless transaction. Accounts the same.");
      return;
    }
    
    if (id1 == null || id2 == null) {
      log.error("File not found");
      return;
    }
    
    String firstAcc = "";
    String secondAcc = "";
    Random rnd = new Random();
    do { // боремся с дедлоком, ждем пока оба файла освободятся
      firstAcc = id1.getValue();
      secondAcc = id2.getValue();

      if (firstAcc == "" && secondAcc != "") {
        
        id2.freeValue();
        secondAcc = "";
        try {
          Thread.sleep(rnd.nextInt(20));
        } catch (InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
      if (firstAcc != "" && secondAcc == "") {
        
        id1.freeValue();
        firstAcc = "";
        try {
          Thread.sleep(rnd.nextInt(20));
        } catch (InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    } while (firstAcc == "" || secondAcc == "");

    try {
      tryTranser(firstAcc, secondAcc, amount);
    } catch (TransferException e) {
      log.warn("Transfer failed. ID{} to ID{} amount={} Reason: {} ",e.getAccountFrom(),e.getAccountTo(),e.getAmount(),e.getMessage());
    }

    id1.freeValue();
    id2.freeValue();

  }

  public void tryTranser(String id1, String id2, Long amount) throws TransferException {

    long startTime = System.currentTimeMillis();
    
    
    AccountParser parser = new AccountParser();
    Account accFrom = parser.parse("accounts//" + id1 + ".json");
    Account accTo = parser.parse("accounts//" + id2 + ".json");
    if (accFrom.getAmount() > amount) {
      transfer(accFrom, accTo, amount);
    } else {
      throw new TransferException("Insufficient funds",id1,id2,amount);
    }
    
    AccountUtils accUtil = new AccountUtils();
    accUtil.newAccount(accFrom);
    accUtil.newAccount(accTo);
    
    long resultTime = System.currentTimeMillis() - startTime;
    log.info("Transaction successful ID{} ID{} amount={} - Transaction_Time = {}ms",id1,id2,amount,resultTime);
  }


  public void transfer(Account accountFrom, Account accountTo, Long amount) {

    //System.out.println(accountFrom.getAmount() + " " + accountTo.getAmount());

    accountFrom.setAmount(accountFrom.getAmount() - amount);
    accountTo.setAmount(accountTo.getAmount() + amount);

    //System.out.println(accountFrom.getAmount() + " " + accountTo.getAmount());

  }

}
