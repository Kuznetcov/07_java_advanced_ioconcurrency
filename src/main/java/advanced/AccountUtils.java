package advanced;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class AccountUtils {

  public void newAccount(Account acc) {

    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
    mapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
    mapper.setSerializationInclusion(Include.NON_EMPTY);
    try (FileOutputStream output = new FileOutputStream("accounts//" + acc.getId() + ".json")) {
      mapper.writeValue(output, acc);
      output.close();
    } catch (JsonGenerationException e) {
      e.printStackTrace();
    } catch (JsonMappingException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void newSerAccount(Account acc) {

    File dir = new File("accounts");
    dir.mkdir();

      File file = new File("serialableAcc//" + acc.getId() + ".bin");
      try (FileOutputStream fos = new FileOutputStream(file)) {
        try (ObjectOutputStream oos = new ObjectOutputStream(fos)) {
          oos.writeObject(acc);
          oos.flush();
        }
      } catch (FileNotFoundException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      }

  }


  public void totalInfo(String source) {

    List<Account> accounts = new ArrayList<Account>();

    AccountParser parser = new AccountParser();
    File f = null;
    File[] paths;
    f = new File(source);
    paths = f.listFiles();
    for (File path : paths) {
      accounts.add(parser.parse(path.toString()));
    }
    Long totalAmount = 0l;
    accounts.stream().forEach((a) -> {
      System.out.println("id" + a.getId() + " " + a.getAmount());
    });
    totalAmount = accounts.stream().mapToLong((a) -> a.getAmount()).sum();
    System.out.println("Общий фонд: " + totalAmount);

  }

  public ConcurrentHashMap<String, Shell> shellUpdate(String source) {

    ConcurrentHashMap<String, Shell> shells = new ConcurrentHashMap<String, Shell>();
    List<Account> accounts = new ArrayList<Account>();
    AccountParser parser = new AccountParser();

    File f = null;
    File[] paths;
    f = new File(source);
    paths = f.listFiles();

    for (File path : paths) {
      accounts.add(parser.parse(path.toString()));
    }
    accounts.stream().forEach((a) -> {
      shells.put(String.valueOf(a.getId()), new Shell(String.valueOf(a.getId())));
    });

    return shells;
  }


  // public void change(Account acc) {
  //
  // ObjectMapper mapper = new ObjectMapper();
  // mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
  // mapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
  // mapper.setSerializationInclusion(Include.NON_EMPTY);
  // try (FileOutputStream output =
  // new FileOutputStream("src//main//resources//" + acc.getId() + ".json")) {
  // mapper.writeValue(output, acc);
  // } catch (JsonGenerationException e) {
  // e.printStackTrace();
  // } catch (JsonMappingException e) {
  // e.printStackTrace();
  // } catch (IOException e) {
  // e.printStackTrace();
  // }
  // }

}
