package advanced;

public class TransferException extends Exception {
  
  /**
   * 
   */
  private static final long serialVersionUID = 3518213485133209070L;
  String accountFrom;
  String accountTo;
  Long amount;
  
  public synchronized String getAccountFrom() {
    return accountFrom;
  }

  public synchronized String getAccountTo() {
    return accountTo;
  }

  public synchronized Long getAmount() {
    return amount;
  }

  public TransferException(String message,String accountFrom, String accountTo, Long amount) {
    super(message);
    this.accountFrom = accountFrom;
    this.accountTo = accountTo;
    this.amount = amount;
  }
 
}
