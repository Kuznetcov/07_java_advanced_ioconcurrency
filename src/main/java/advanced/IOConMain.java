package advanced;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class IOConMain {

  public static void main(String[] args) {

    AccountUtils create = new AccountUtils();
    Random rnd = new Random();

    for (int i = 1; i < 16; i++) {
      create.newAccount(new Account(i, Long.valueOf(rnd.nextInt(20000))));
    }

    // for (int i = 1; i < 16; i++) {
    // create.newSerAccount(new Account(i, Long.valueOf(rnd.nextInt(20000))));
    // }

    ConcurrentHashMap<String, Shell> accounts = new ConcurrentHashMap<String, Shell>();

    create.totalInfo("accounts//");
    accounts = create.shellUpdate("accounts//");

    ExecutorService executorService = Executors.newFixedThreadPool(10);

    for (int i = 0; i < 1000; i++) {
      String acc1 = String.valueOf(rnd.nextInt(accounts.size()) + 1);
      String acc2 = String.valueOf(rnd.nextInt(accounts.size()) + 1);
      executorService.execute(
          new Transaction(accounts.get(acc1), accounts.get(acc2), Long.valueOf(rnd.nextInt(100))));
    }

    executorService
        .execute(new Transaction(accounts.get(String.valueOf(rnd.nextInt(accounts.size()) + 1)),
            accounts.get(String.valueOf(rnd.nextInt(accounts.size()) + 1)), 100000l));


    executorService.shutdown();

    while (true) {
      if (executorService.isTerminated()) {
        create.totalInfo("accounts//");
        break;
      } else
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
    }
  }
}
