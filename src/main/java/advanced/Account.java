package advanced;

import java.io.Serializable;

public class Account implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1286723206195790862L;
  public Account() {
    super();
  }
  public Account(Integer id, Long amount) {
    super();
    this.id = id;
    this.amount = amount;
  }
  public synchronized Integer getId() {
    return id;
  }
  public synchronized void setId(Integer id) {
    this.id = id;
  }
  public synchronized Long getAmount() {
    return amount;
  }
  public synchronized void setAmount(Long amount) {
    this.amount = amount;
  }
  
  private Integer id;
  private Long amount;
  
  
}
