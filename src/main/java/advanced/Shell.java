package advanced;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Shell {

  String value;
  Boolean flag;
  Lock lock;

  public Shell(String shell) {

    this.value = shell;
    this.flag = true;
    this.lock = new ReentrantLock();

  }

  public String getValue() {

    if (this.lock.tryLock()) {
      this.flag = false;
      return this.value;
    } else
      return "";
  }


  public void freeValue() {

    this.flag = true;
    this.lock.unlock();

  }

}
